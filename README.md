# Humans of Animafac Zenphoto theme

[Zenphoto](https://www.zenphoto.org/) theme used on [humansof.animafac.net](https://humansof.animafac.net/Humans-of-Animafac/)

## Setup

The theme needs to be installed in the `themes/` directory of Zenphoto.

You then need to use [Bower](https://bower.io/) to install the dependencies:

```bash
bower install
```

You also need to enable the theme in Zenphoto settings.
