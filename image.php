<?php
// force UTF-8 Ø

if (!defined('WEBPATH'))
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php echo LOCAL_CHARSET; ?>">
		<?php zp_apply_filter('theme_head'); ?>
		<?php printHeadTitle(); ?>
        <link rel="stylesheet" href="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/bower_components/jquery-ui/themes/ui-lightness/jquery-ui.css" />
		<link rel="stylesheet" href="<?php echo pathurlencode($zenCSS); ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/common.css" type="text/css" />
        <script src="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/bower_components/jquery-ui/jquery-ui.min.js"></script>
		<?php if (zp_has_filter('theme_head', 'colorbox::css')) { ?>
			<script type="text/javascript">
				// <!-- <![CDATA[
				$(document).ready(function() {
					$(".colorbox").colorbox({
						inline: true,
						href: "#imagemetadata",
						close: '<?php echo gettext("close"); ?>'
					});
					$(".fullimage").colorbox({
						maxWidth: "98%",
						maxHeight: "98%",
						photo: true,
						close: '<?php echo gettext("close"); ?>'
					});
				});
				// ]]> -->
			</script>
		<?php } ?>
		<?php if (class_exists('RSS')) printRSSHeaderLink('Gallery', gettext('Gallery RSS')); ?>
        <meta property="og:title" content="<?php printImageTitle(); ?> - Humans of Animafac" />
        <meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" />
        <meta property="og:image" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].htmlspecialchars(getFullImageURL()); ?>" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@Animafac" />
        <meta property="og:description" content="<?php printImageDesc(); ?>" />
	</head>
	<body>
		<?php zp_apply_filter('theme_body_open'); ?>
		<div id="main">
			<div id="gallerytitle">
				<div class="imgnav">
					<?php
					if (hasPrevImage()) {
						?>
						<div class="imgprevious"><a href="<?php echo html_encode(getPrevImageURL()); ?>" title="<?php echo gettext("Previous Image"); ?>">« <?php echo gettext("prev"); ?></a></div>
						<?php
					} if (hasNextImage()) {
						?>
						<div class="imgnext"><a href="<?php echo html_encode(getNextImageURL()); ?>" title="<?php echo gettext("Next Image"); ?>"><?php echo gettext("next"); ?> »</a></div>
						<?php
					}
					?>
				</div>
                <?php include __DIR__.'/h1.php'; ?>
				<h2>
					<span>
						<?php printHomeLink('', ' | '); printGalleryIndexURL(' | ', getGalleryTitle());
						printParentBreadcrumb("", " | ", " | ");
						printAlbumBreadcrumb("", " | ");
						?>
					</span>
					<?php printImageTitle(); ?>
				</h2>
			</div>
			<!-- The Image -->
			<div id="image">
				<strong>
					<?php
					if (isImagePhoto()) {
						$fullimage = getFullImageURL();
					} else {
						$fullimage = NULL;
					}
					if (!empty($fullimage)) {
						?>
						<a href="<?php echo html_encode(pathurlencode($fullimage)); ?>" title="<?php printBareImageTitle(); ?>" class="fullimage">
							<?php
						}
						if (function_exists('printUserSizeImage') && isImagePhoto()) {
							printUserSizeImage(getImageTitle());
						} else {
							printDefaultSizedImage(getImageTitle());
						}
						if (!empty($fullimage)) {
							?>
						</a>
						<?php
					}
					?>
				</strong>
				<?php
				if (isImagePhoto())
					@call_user_func('printUserSizeSelector');
				?>
			</div>
			<div id="narrow">
				<?php printImageDesc(); ?>
                <div class="desc-author"><?php printImageTitle(); ?></div>
				<hr /><br />
				<?php
				If (function_exists('printAddToFavorites'))
					printAddToFavorites($_zp_current_image);
				@call_user_func('printSlideShowLink');

				if (getImageMetaData()) {
					printImageMetadata(NULL, 'colorbox_meta');
					?>
					<br class="clearall" />
					<?php
				}
				printTags('links', gettext('<strong>Tags:</strong>') . ' ', 'taglist', '');
				?>
				<br class="clearall" />

				<?php
    @call_user_func('printGoogleMap');
    @call_user_func('printRating');
    @call_user_func('printCommentForm');
    ?>
			</div>
            <div id="cta">
                <div class="cta-title">Je veux...</div>
                <div id="tabs">
                  <ul>
                    <li><a href="#tabs-1">Partager ma photo</a></li>
                    <li><a href="#tabs-2">Booster mon asso</a></li>
                    <li><a href="#tabs-3">Relayer la campagne</a></li>
                    <li><a href="#tabs-4">M'engager dans une asso</a></li>
                  </ul>
                  <div id="tabs-1">
                      <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" class="button">Facebook</a><br/><br/>
                      <a target="_blank" href="https://twitter.com/home?status=%23HumansOfAnimafac%20%40animafac%20<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" class="button">Twitter</a><br/>
                  </div>
                  <div id="tabs-2">
                      <a target="_blank" href="http://www.animafac.net/adherer-au-reseau" class="button">En adhérant à Animafac</a><br/><br/>
                      <a target="_blank" href="http://www.animafac.net/sabonner-newsletter" class="button">En m’abonnant à la newsletter d’Animafac</a><br/><br/>
                      <a target="_blank" href="mailto:coordination@animafac.net" class="button">En étant contacté.e pour participer à des événements locaux avec d’autres assos</a><br/><br/>
                      <a target="_blank" href="https://www.facebook.com/events/225369957808298/" class="button">En suivant l’émission Fac’Stories le 9 avril</a><br/><br/>
                      <a target="_blank" href="http://www.animafac.net/actualites/20-ans-journee-quallons-faire-20-ans" class="button">En participant à la journée des 20 ans d’Animafac</a><br/>
                  </div>
                  <div id="tabs-3">
                      <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" class="button">Partager cette photo sur les réseaux sociaux</a><br/><br/>
                      <a target="_blank" href="http://www.animafac.net/actualites/20-ans-participer-campagne" class="button">Télécharger le kit de campagne</a><br/><br/>
                      <a target="_blank" href="http://www.animafac.net/actualites/20-ans-journee-quallons-faire-20-ans" class="button">S’inscrire à la journée des 20 ans d’Animafac</a><br/>
                  </div>
                  <div id="tabs-4">
                      <a target="_blank" href="mailto:coordination@animafac.net
" class="button">Rejoindre une asso étudiante de ma ville</a><br/><br/>
                      <a target="_blank" href="http://www.animafac.net/guides-pratiques/le-guide-du-porteur-de-projets" class="button">Créer mon asso</a><br/><br/>
                      <a target="_blank" href="http://www.service-civique.gouv.fr/" class="button">Postuler à une mission de Service civique</a><br/>
                  </div>
                </div>
                <script>
                $(function() {
                  $( "#tabs" ).tabs();
                  $( ".button" ).button()
                });
                </script>
            </div>
		</div>
		<div id="credit">
			<?php
			if (function_exists('printFavoritesURL')) {
				printFavoritesURL(NULL, '', ' | ', '<br />');
			}
			?>
			<?php
   if (class_exists('RSS')) printRSSLink('Gallery', '', 'RSS', ' | ');
   printCustomPageURL(gettext("Archive View"), "archive"); ?> |
			<?php printZenphotoLink();
   @call_user_func('printUserLogin_out', " | ");
   ?>
		</div>
		<?php
		zp_apply_filter('theme_body_close');
		?>
	</body>
</html>
