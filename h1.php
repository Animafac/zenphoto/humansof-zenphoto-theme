<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73974332-1', 'auto');
  ga('send', 'pageview');
</script>
<div class="header">
    <div>
        <?php
            echo '<img src="'.$_zp_themeroot.
                '/images/logo-animafac-baseline.png" alt="Animafac" />';
        ?>
    </div>
    <h1>
        <?php
            echo '<a href="'.FULLWEBPATH.'">Humans of Animafac</a>';
        ?>
    </h1>
</div>
